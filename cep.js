$('#cep').on('focusout', function(event){
    console.log($('#cep').val());
    let ajax = $.ajax({
        url: `https://api.postmon.com.br/v1/cep/${$('#cep').val()}`
    })
    ajax.done(function(infosJson){
        console.log(infosJson)
        $('#rua').val(infosJson.logradouro);
        $('#bairro').val(infosJson.bairro);
        $('#cidade').val(infosJson.cidade);
        $('#estado').val(infosJson.estado);
    })
    ajax.fail(function(){
        console.log('fail')
    })
});